#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Extract argument
directory=$1

# Check if the directory exists
if [ ! -d "$directory" ]; then
    echo "Error: Directory '$directory' not found"
    exit 1
fi

# Delete files starting with 'file_'
find "$directory" -type f -name "file_*" -delete

echo "Deleted files starting with 'file_' from $directory"
