#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <number_of_files> <output_directory>"
    exit 1
fi

# Extract arguments
num_files=$1
output_dir=$2

# Create output directory if it doesn't exist
mkdir -p "$output_dir"

# Create n files in the output directory
for ((i = 0; i < num_files; i++)); do
    touch "$output_dir/file_$i.txt"
done

echo "Created $num_files files in $output_dir"
